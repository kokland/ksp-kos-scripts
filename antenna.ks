SET part TO SHIP:PARTSNAMED("longantenna")[0].
SET m to part:GETMODULE("ModuleRTAntenna").
print " -- activating antenna".
m:DOEVENT("activate").
print " -- setting target to mission control".
m:SETFIELD("target", "mission-control").

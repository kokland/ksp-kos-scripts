
lock throttle to 1. //Throttle is a decimal from 0.0 to 1.0
gear off.

set targetApoapsis to 750000. //Target apoapsis in meters
set targetPeriapsis to 750000. //Target periapsis in meters

set runmode to 1.

SET mysteer TO HEADING(00,90).

set throttlevalue to 1.
lock throttle to throttlevalue.
wait 0.5.



WHEN MAXTHRUST = 0 THEN {
	    PRINT "Staging".
	    STAGE.
	    wait 0.5.
	    preserve.
	}.

until runmode = 0 {
	when runmode > 0 then {
		print "runmode: " + runmode at(10,30).
		print "altitude: " + round(ship:altitude) at (10,31).
		print "apoapsis: " + round(ship:apoapsis) at (10,32).
    print "throttle: " + round(throttlevalue*100) at (10,33).
		preserve.
	}.






	if runmode = 1 {
		print "runmode 1".
		lock throttle to 1.
		LOCK STEERING TO mysteer.
		stage.
		clearscreen.
		set runmode to 2.
	}.


  //takeoff
	if runmode = 2 {
		print "runmode 2".
		set mysteer to heading(0, 90).
    until altitude > 8000 {
      if ship:velocity:surface:mag >= 240 {
        if throttlevalue > 0.2 {
          set throttlevalue to throttlevalue - 0.01.
          lock throttle to throttlevalue.
        }
      }.
      if ship:velocity:surface:mag < 240 {
        if (throttlevalue <= 1) {
          set throttlevalue to throttlevalue + 0.01.
        }.
      }.
			CheckIfWeShouldStage().
    }.

		set runmode to 3.
	}.

  // ascent
	if runmode = 3 {
		print "runmode 3".
		until altitude > 10000 {
      if ship:velocity:surface:mag >= 250 {
        set throttlevalue to throttlevalue - 0.01.
      }.
      if ship:velocity:surface:mag < 250 {
        set throttlevalue to throttlevalue + 0.2.
      }.
    }.
		SetPitch(10000, 0, 80).
		SetPitch(15000, 0, 75).
		SetPitch(20000, 0, 70).
		SetPitch(30000, 0, 60).
		SetPitch(40000, 0, 50).
		SetPitch(50000, 0, 35).
		SetPitch(targetApoapsis, 0, 3).

    until ship:apoapsis > targetApoapsis {
			print "waiting for apoapsis: " + ship:apoapsis at(10,29).
			set throttlevalue to 1.
			wait 0.1.
    }.

		CoastToApoapsis().




	}.

  // orbital burn
  if runmode = 4 {
    OrbitalBurn().
  }.

	if runmode = 5 {
		Circularize().
	}
}.

lock steering to heading(-90, 0).



declare function SetPitch {
	parameter altitude.
	parameter heading.
	parameter pitch.

	if ship:altitude > 35000 {
		rcs on.
		sas on.
	}

	set mysteer to heading(heading, pitch).
	set throttlevalue to 1.
	until apoapsis > altitude {

		CheckIfWeShouldStage().
	}.
}.

// OrbitalBurn
declare function OrbitalBurn {
	clearscreen.
	print "orbital burn" at (10,35).
	set mysteer to heading(0, 0).
	lock throttle to 1.
	until periapsis > targetPeriapsis * 0.98 {
		set mysteer to heading(180, 0).
		wait 0.1.
	}.
	lock throttle to 0.
	set runmode to 0.
}.

declare function Circularize {
	print "circularizing" at(10,35).
	wait until eta:periapsis < 5.
	set mysteer to heading(90, 0).
	set throttlevalue to 1.
	until ship:periapsis = apoapsis*0.95 {
		set mysteer to heading(0, 0).
		wait 0.1.
	}
	set throttlevalue to 0.
	set mysteer to down.
	set runmode to 5.
}.

// CoastToApoapsis
declare function CoastToApoapsis {
	set throttlevalue to 0.
	set warp to 4.
	wait 2.
	print "coasting to space".
	wait until ship:altitude > 70000.
	set warp to 0.
	wait 2.
	print "warping to apoapsis".
	set warp to 1.
	set mysteer to heading(180,0).
	wait until eta:apoapsis < 10.
	set warp to 0.
	set runmode to 4. // orbital burn
}.


declare function CheckIfWeShouldStage {

  list engines in englist.
  for eng in englist {
    if eng:stage = stage:number {
      if eng:fuelflow < 0.001 {
        print "no fuelflow for an engine: " + eng.
        stage.

				wait 0.5.
      }.
    }.
  }.
}.


print "program ended...".

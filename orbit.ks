
lock throttle to 1. //Throttle is a decimal from 0.0 to 1.0
gear off.

set targetApoapsis to 120000. //Target apoapsis in meters
set targetPeriapsis to 120000. //Target periapsis in meters

set runmode to 1.

SET mysteer TO HEADING(90,90).

set throttlevalue to 1.
lock throttle to throttlevalue.
wait 0.5.



WHEN MAXTHRUST = 0 THEN {
	    PRINT "Staging".
	    STAGE.
	    wait 0.5.
	    preserve.
	}.

until runmode = 0 {
	when runmode > 0 then {
		print "runmode: " + runmode at(10,30).
		print "altitude: " + round(ship:altitude) at (10,31).
		print "apoapsis: " + round(ship:apoapsis) at (10,32).
    print "throttle: " + round(throttlevalue*100) at (10,33).
		preserve.
	}.






	if runmode = 1 {
		print "runmode 1".
		lock throttle to 1.
		LOCK STEERING TO mysteer.
		stage.
		clearscreen.
		set runmode to 2.
	}.


  //takeoff
	if runmode = 2 {
		print "runmode 2".
    wait until ship:velocity:surface:mag > 100.
    set mysteer to heading(90, 85).
    until altitude > 8000 {
      if ship:velocity:surface:mag >= 240 {
        if throttlevalue > 0.2 {
          set throttlevalue to throttlevalue - 0.01.
          lock throttle to throttlevalue.
        }
      }.
      if ship:velocity:surface:mag < 240 {
        if (throttlevalue <= 1) {
          set throttlevalue to throttlevalue + 0.01.
        }.
      }.
			CheckIfWeShouldStage().
    }.

		set runmode to 3.
	}.

  // ascent
	if runmode = 3 {
		print "runmode 3".
		until altitude > 10000 {
      if ship:velocity:surface:mag >= 300 {
        set throttlevalue to throttlevalue - 0.01.
      }.
      if ship:velocity:surface:mag < 300 {
        set throttlevalue to throttlevalue + 0.01.
      }.
      CheckIfWeShouldStage().
    }.
		SetPitch(10000, 90, 80).
    SetPitch(14000, 90, 75).
		SetPitch(18000, 90, 70).
		SetPitch(22000, 90, 65).
		SetPitch(26000, 90, 60).
		SetPitch(30000, 90, 55).
		SetPitch(34000, 90, 50).
    SetPitch(38000, 90, 45).
    SetPitch(42000, 90, 40).
    SetPitch(48000, 90, 35).
    SetPitch(52000, 90, 30).
    SetPitch(56000, 90, 25).
    SetPitch(58000, 90, 20).
		SetPitch(targetApoapsis, 90, 5).

    until ship:apoapsis > targetApoapsis {
			print "waiting for apoapsis: " + ship:apoapsis at(10,29).
			set throttlevalue to 1.
			wait 0.1.
    }.

		CoastToApoapsis().




	}.

  // orbital burn
  if runmode = 4 {
    OrbitalBurn().
  }.

	if runmode = 5 {
		Circularize().
	}
}.

lock steering to heading(90, 0).



declare function SetPitch {
	parameter altitude.
	parameter heading.
	parameter pitch.

	if ship:altitude > 35000 {
		rcs on.
		sas on.
	}

  print "current heading: " + heading at(10,25).
  print "current pitch: " + pitch at(10, 26).
  set throttlevalue to 1.

	set mysteer to heading(heading, pitch).
  wait until apoapsis > altitude - 2000.
  set mysteer to heading(heading, pitch - 2).

  print "current heading: " + heading at(10,25).
  print "current pitch: " + (pitch - 2) at(10, 26).

	until apoapsis > altitude {

		CheckIfWeShouldStage().
	}.
}.

// OrbitalBurn
declare function OrbitalBurn {
	clearscreen.
	print "orbital burn" at (10,35).
  sas on.
  rcs on.
	set mysteer to heading(90, 0).
  wait until eta:apoapsis < 3.
	lock throttle to 1.
	until periapsis > targetPeriapsis * 0.98 {
		set mysteer to heading(90, 0).
		wait 0.1.
	}.
	lock throttle to 0.
	set runmode to 0.
}.

declare function Circularize {
	print "circularizing" at(10,35).
	wait until eta:periapsis < 5.
	set mysteer to heading(90, 0).
	set throttlevalue to 1.
	until ship:periapsis = apoapsis*0.95 {
		set mysteer to heading(90, 0).
		wait 0.1.
	}
	set throttlevalue to 0.
	set mysteer to down.
	set runmode to 5.
}.

// CoastToApoapsis
declare function CoastToApoapsis {
	set throttlevalue to 0.
	set warp to 4.
	wait 2.
	print "coasting to space".
	wait until ship:altitude > 70000.
	set warp to 0.
	wait 2.
	print "warping to apoapsis".
	set warp to 3.
  wait until eta:apoapsis < 60.
  set warp to 1.
	wait until eta:apoapsis < 30.
  set warp to 0.
	set runmode to 4. // orbital burn
}.


declare function CheckIfWeShouldStage {

  list engines in englist.
  for eng in englist {
    if eng:stage = stage:number {
      if eng:fuelflow < 0.001 {
        print "no fuelflow for an engine: " + eng.
        stage.

				wait 0.5.
      }.
    }.
  }.
}.


print "program ended...".

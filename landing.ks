/// stop horizontal velocity
sas on.
rcs on.
wait 0.1.
set tval to 0.
lock throttle to tval.

//lock steering to  retrograde.
set sasmode to "retrograde".
wait 3.

print "killing horizontal velocity".
until ship:groundspeed < 10 {
  lock throttle to 1.
  wait 0.01.
}.

lock throttle to 0.


// phase 3: landing
set desiredvelocity to 200.
set t to 0.
lock throttle to t.

until CorrectGroundLevel() < 5 {
  if (CorrectGroundLevel()  < 10) {
    set desiredvelocity to 2.
    set sasmode to "stabilityassist".
  }
  else if (CorrectGroundLevel() < 50) {
    set desiredvelocity to 5.
  }
  else if (CorrectGroundLevel() < 500) {
    set desiredvelocity to 10.
  }
  else if (CorrectGroundLevel() < 1000) {
    set desiredvelocity to 20.
  }
  else if (CorrectGroundLevel() < 1500) {
    set desiredvelocity to 50.
  }
  else if (CorrectGroundLevel() < 2500) {
    set desiredvelocity to 75.
  }
  else if (CorrectGroundLevel() < 5000) {
    set desiredvelocity to 100.
  }
  else if (CorrectGroundLevel() < 10000) {
    set desiredvelocity to 125.
  }
  else if (CorrectGroundLevel() < 15000) {
    set desiredvelocity to 150.
  }

  if (ship:velocity:surface:mag > desiredvelocity) {
    set t to min(1, t + 0.01).
  }
  else {
    set t to max(0, t - 0.01).
  }
  wait 0.001.
  print "current altitude: " + CorrectGroundLevel() at (10,30).
}


print "killing engines".
lock throttle to 0.
wait 3.
unlock steering.
unlock throttle.


declare function CorrectGroundLevel {
  //return round(altitude - ship:geoposition:terrainheight).
  return alt:radar.
}.
